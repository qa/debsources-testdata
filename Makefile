DUMPS = \
	db/pg-dump-plain \
	db/pg-dump-custom \
	$(NULL)

PG_DUMP_FLAGS = --schema public --no-owner --no-privileges
PG_RESTORE_FLAGS = --single-transaction

REAL_DB_NAME = debsources
TEST_DB_NAME = debsources-test

all:

dump: $(DUMPS)

restore-test: $(DUMPS)
	pg_restore --dbname $(TEST_DB_NAME) db/pg-dump-custom

db/pg-dump-plain:
	pg_dump $(PG_DUMP_FLAGS) $(REAL_DB_NAME) > $@

db/pg-dump-custom:
	pg_dump --format custom $(PG_DUMP_FLAGS) $(REAL_DB_NAME) > $@

distclean:
	rm -f $(DUMPS)
