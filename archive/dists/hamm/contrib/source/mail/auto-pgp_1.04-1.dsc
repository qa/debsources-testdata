-----BEGIN PGP SIGNED MESSAGE-----

Source: auto-pgp
Version: 1.04-1
Binary: auto-pgp
Maintainer: Dirk Eddelbuettel <edd@debian.org>
Architecture: any
Standards-Version: 2.4.1
Files: 
 130b4b019894e90c42164dab50665141 26866 auto-pgp_1.04.orig.tar.gz
 dee0cd5944155adbb4482c2c4285ba6d 4839 auto-pgp_1.04-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.3ia
Charset: latin1
Comment: Please `finger edd@master.debian.org' for my public key.

iQCVAwUBNU54PXd1UC9tfwjdAQH23gQAqfhpT/F48B0/yrD3gkH8SFSirrLGEahH
XDSYsHp+A/P2uYMKsAQMf6heLvBSXdqTiv9hU4njwi6yV6tfB4P+mWTWpL+DrBB1
M1I55Rj3hgbFfct9Yy+ILC4GrsSeOZ+fOcIQi1Xy5csyMd7GcviCPKPdkaI07lTn
dcf1G6GOb4c=
=HMVz
-----END PGP SIGNATURE-----
