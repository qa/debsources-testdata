-----BEGIN PGP SIGNED MESSAGE-----

Source: ed
Version: 0.2-16
Binary: ed
Maintainer: James Troup <jjtroup@comp.brad.ac.uk>
Architecture: any
Standards-Version: 2.4.0.0
Files: 
 ddd57463774cae9b50e70cd51221281b 185913 ed_0.2.orig.tar.gz
 98c91e3cafac69e7f25241d9521dc36c 3921 ed_0.2-16.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.3ia
Charset: noconv

iQCVAwUBNPtNqHaw9XG4JAR1AQHU+QQAjRfdBztaO8oF9uUg/zh9KJ3BC2dYfo4t
6suVfIlFuPpOENLr7+dhZZb0ob+SsPzNIa8SEQtk+Z/8O/BtqpPEnxGvanv4mJFM
0wtfBLT6W1FhB662dX523avk5vwqnmmMQntudkLsKmgqdo+l1iy9dgskdQowqpkO
92QGWNcrR9k=
=eJYr
-----END PGP SIGNATURE-----
