-----BEGIN PGP SIGNED MESSAGE-----

Source: freewrl
Version: 0.20.a1-3
Binary: freewrl
Maintainer: Stephen Zander <gibreel@debian.org>
Architecture: any
Standards-Version: 3.0.1
Files: 
 99d4b5453aa71718b41b1bdf2ee90c21 1565579 freewrl_0.20.a1.orig.tar.gz
 ff5177d781158cd17b85029e10be7502 7057 freewrl_0.20.a1-3.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.3ia
Charset: noconv

iQCVAwUBOCPjSGNl6w3rzdpRAQFmxwP9Eb5kSIP4b7nFJ2qT4ZhrSpWpWUZc1of7
7Ig0hsAZYZEqOilkjNjMx5QKvuIT3GywxZFs9neJw4aVf6SJCyK1CjuyKRKDGPlg
W0UdQWvuRGApqnvbOX4QYPYhS0YmSTfJ2r8DNFgkK0VAtaQO/6uv0yP04XR4YqHz
dGjig158Wgo=
=PsFn
-----END PGP SIGNATURE-----
