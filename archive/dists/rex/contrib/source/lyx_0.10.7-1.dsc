-----BEGIN PGP SIGNED MESSAGE-----

Source: lyx
Version: 0.10.7-1
Binary: lyx
Maintainer: Stuart Lamble <lamble@yoyo.cc.monash.edu.au>
Architecture: any
Standards-Version: 2.1.0.0
Files: 
 1cda21a9b54fba754618bb28c9e1f25a 612506 lyx_0.10.7.orig.tar.gz
 f59beba9d5bcee8524dc319fb84d5fac 6974 lyx_0.10.7-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.2i

iQCVAwUBMmil+NancbR/eGtJAQHzHAP+Oof8zXviEndiBzmAowMgzARG/pLacwAF
7VVZrHz4vunqaeALwEdFLvvCFWditJUauv9QIooAhf9gCZy+K4NTc37wHuQfF+dO
r9WklJVflNoxLudZ7oJWc54ViAXjxENPoUIstds/uYWLktedYWYEVD0nEfIoPRIe
cmcEZ6dWz8c=
=yIUv
-----END PGP SIGNATURE-----
