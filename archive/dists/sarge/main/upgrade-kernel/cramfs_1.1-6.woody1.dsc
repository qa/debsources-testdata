Format: 1.0
Source: cramfs
Version: 1.1-6.woody1
Binary: cramfsprogs
Maintainer: Debian kernel team <debian-kernel@lists.debian.org>
Architecture: any
Standards-Version: 3.6.1
Build-Depends: debhelper (>= 3), libz-dev
Uploaders: Francesco Paolo Lovergine <frankie@debian.org>
Files: 
 d3912b9f7bf745fbfea68f6a9b9de30f 24179 cramfs_1.1.orig.tar.gz
 e4934192f6c88a3d042ac4c8d8a5a732 4805 cramfs_1.1-6.woody1.diff.gz
