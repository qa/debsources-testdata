-----BEGIN PGP SIGNED MESSAGE-----

Source: tripwire
Version: 1.2-15
Binary: tripwire
Maintainer: Michael Meskes <meskes@debian.org>
Architecture: any
Standards-Version: 2.4.0.0
Files: 
 6a26d6604150778533b1a74c6179e518 293914 tripwire_1.2.orig.tar.gz
 8d8ceec7877b0bb02a47584dd903fe41 12015 tripwire_1.2-15.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.3ia
Charset: noconv

iQCVAwUBNizh4GNl6w3rzdpRAQEsBgP+ObxBgK6bfUjsswuqKsK7vvEA/PgMjZZy
+/ckJGgVUBXI8Ged3Ppzwf7+kkjHftYoHHIjSdy5trwt6PQ1a2JrOTawmWLncVN7
8sjhOlCO/LcQNqM2SNb/qkZwZr5KguCSkh5wW/rvHekg5qHODgfxdYtbeAC4Jvdt
dim4zlGuqx8=
=l0z0
-----END PGP SIGNATURE-----
