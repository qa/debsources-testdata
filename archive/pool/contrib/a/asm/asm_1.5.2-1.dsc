-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: asm
Version: 1.5.2-1
Binary: libasm-java, libasm-java-doc
Maintainer: Marcus Crafter <crafterm@debian.org>
Architecture: all
Standards-Version: 3.6.1.1
Build-Depends-Indep: debhelper (>= 4.0.0), ant, sharutils, j2sdk | java2-compiler
Files: 
 15d7ff652bc929612e83e595a4b91cd8 149059 asm_1.5.2.orig.tar.gz
 84209104f63bb4e9976481d56f157324 22986 asm_1.5.2-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.5 (GNU/Linux)

iD8DBQFBxKzXOU0v48TFe0IRAiV2AKCef10lcyDZ4zHw+r1pG47DItPxyACeLX3A
GMexvnfHK+Yiph3+DnXx0W0=
=ioKI
-----END PGP SIGNATURE-----
