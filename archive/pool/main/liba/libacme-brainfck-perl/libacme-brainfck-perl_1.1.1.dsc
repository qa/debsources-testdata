-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: libacme-brainfck-perl
Version: 1.1.1
Binary: libacme-brainfck-perl
Maintainer: Jaldhar H. Vyas <jaldhar@debian.org>
Architecture: all
Standards-Version: 3.6.1
Build-Depends: debhelper (>= 3.0.5), perl (>= 5.6.0-17), libmodule-build-perl
Files: 
 17c824faa90cc478b0e3e13997938355 8266 libacme-brainfck-perl_1.1.1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.4 (GNU/Linux)

iD8DBQFActeA2kYOR+5txmoRAm17AKCE0t3zLWPwLLCBukF96l97c31PjwCaAhg7
0PwwL/ei64fUAtU7mVn8o9U=
=ATRm
-----END PGP SIGNATURE-----
