-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: libacme-poe-knee-perl
Version: 1.10-5
Binary: libacme-poe-knee-perl
Maintainer: Steve Kowalik <stevenk@debian.org>
Architecture: all
Standards-Version: 3.6.1
Build-Depends-Indep: debhelper (>= 4), perl (>= 5.6.0-17)
Files: 
 38dae3405e6c92da467748f9447d4d77 3838 libacme-poe-knee-perl_1.10.orig.tar.gz
 6357b36ce18a8445d5bbb1ea329e77a4 7502 libacme-poe-knee-perl_1.10-5.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.4 (GNU/Linux)

iD8DBQFAMyq4CfB0CMh//C8RAkyjAKC5+RuTzuxz38JNYaICYvNxJw8B0ACeILBJ
Z+MUZliHhdx0L/8SkwjcrTc=
=4+VX
-----END PGP SIGNATURE-----
