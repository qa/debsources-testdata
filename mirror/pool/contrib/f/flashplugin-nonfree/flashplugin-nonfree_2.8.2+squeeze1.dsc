-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 1.0
Source: flashplugin-nonfree
Binary: flashplugin-nonfree
Architecture: i386 amd64
Version: 1:2.8.2+squeeze1
Maintainer: Bart Martens <bartm@debian.org>
Homepage: http://wiki.debian.org/FlashPlayer
Standards-Version: 3.8.3
Build-Depends: debhelper (>= 5), cdbs
Checksums-Sha1: 
 e8c3666c52c7731f8fbcd309240e37661b1691fc 17006 flashplugin-nonfree_2.8.2+squeeze1.tar.gz
Checksums-Sha256: 
 d9508f19c2596249292f4c479359b6ab0b0c79c4c66f900bc4942e851a0243b6 17006 flashplugin-nonfree_2.8.2+squeeze1.tar.gz
Files: 
 51f37a34f79b5aad7966e077d296d65f 17006 flashplugin-nonfree_2.8.2+squeeze1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iQIcBAEBCAAGBQJQy2xrAAoJEDNV9NY7WCHMH0EQAIRx4ppG8+5vjBS3FbsMpC8Q
jWPl0eWCm3UXtIC2rG/pNFuNwryeOTzv4/+8hFfzsmtGkUrrQlt0/aVlRZ6vRbNy
Aa8DR7Lg+bLWO4NxgR/72qdYPvgeVIjfVxRo24CkSmnWw/CZtVK3BYIqhI6uB77a
6WT/SgDK96x84P1AxyiEbgHyauvyMccoXBgW4vHQ6LtUFl8Iw3PO2drl13PSJzbj
y3nUuPJS91l6dm/zt30ybzEPQfR+Pt6ZBkSVmXmzLt2UkJqUaZNNdzTu8UdH9sFY
E5cObuD6RRhzQTuG36+N2AU22efgHqsP6vTAaQL81nR21GfijXKI7wEkZocsUEE1
pA3yoc/Xgz8ssWZYdD6kXItW7y2rQt/M7FzFuz1JdS1QEgmDI22qBdZgMPM6l+SW
ZWTRBU3grb4bflUm3tdllHpalPAk1shy7EyOhuOYHVyp0UER40gL8AA5wa1SGoJq
rL9yG0pDtpqCXiQM052q0cCA17FwPVOLPrnTITlWuG+qOkFIb9Ht817EhMAr9ASc
YHUqheyeodDzZK7fmVzoFmnNHIodRdGHVfp3YB0AKsP4m6yhvdoNJc/7jhpQF0XK
wDRapLM2Q7v5R6qTPL0gTDzHw+vIqUnjHQJdBRhTdqokCALkE5KzX+TUVNd0dLWW
5mVKS4zp0RaV2j0qxmQ2
=Yi3V
-----END PGP SIGNATURE-----
