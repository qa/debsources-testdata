-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: flashplugin-nonfree
Binary: flashplugin-nonfree
Architecture: i386 amd64
Version: 1:3.2
Maintainer: Bart Martens <bartm@debian.org>
Homepage: http://wiki.debian.org/FlashPlayer
Standards-Version: 3.9.3
Build-Depends: debhelper (>= 9)
Package-List: 
 flashplugin-nonfree deb contrib/web optional
Checksums-Sha1: 
 069b0f62339782f1b1269c79cc272cba8f0107b9 19514 flashplugin-nonfree_3.2.tar.gz
Checksums-Sha256: 
 ddd4c08b8812ac4caaa4dec439b6d550582a62ae60f2c800fae3d5f363bef2f5 19514 flashplugin-nonfree_3.2.tar.gz
Files: 
 d4a271fa8c45082a5885a01840aab4ec 19514 flashplugin-nonfree_3.2.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iQIcBAEBCAAGBQJQykaRAAoJEDNV9NY7WCHMKf4QAMBd/g0UFT4podj/ydVcXHEK
Kpgujk/nEYJPCCto116y3fYwjHN7OwpFZUhBqo7/93nz4Ax7mPGrgCTa/34DDf/B
bRWbwsr7nn1tBL4f9HmjyDO10G9imPEF2PUkjK5cPacf1PF7hfY88dihZQUPLndc
k1L4hQUv5Ft2J0DmNPeJPRz0IHZbXSthA5jQMvp9KZzU0h+kXGEaFkVmyDAGVFpq
e/Rn3KRg5qBSAUIQFzUDQV21+r338Ty35iVuggw8pvkxjaP0/+QGocYzQ1S8EbhN
zV47E7TZBnpwTpq3MLTKjS+O6WlWbOyQkSk81/is3NjM/ZaAlSxFmTA5BjV3a8o5
sUGb1AbEfFEePz+14L1srWrpQto1Jx3hmpufIev/P82GEhhbl9ICz9D/HDNBTYtp
NNERNF1on4jlj9+qp/OXjWKKDq60M2CpDwjQziNwLeDDZkX7duvdlDfQWfHByl2h
mK3vfKhDRA0SxAzrVss5BJCmcXr0AwQTkm1pxwfn1T/EsTRzaW/gZTfqjpekHa5w
+4IDAjXuFTUu1mQks+DJrGcCWdD7x1GztwDKF6VA46TG37kl2VUYSH2vsCJ4U/5l
dLA41Emca4HYETebye5K+suhxNIT6pjIL/uTWS//hNFnylcY7vA+73vFqLB/Qq7c
xQBR/9+BvL4zyqNK2LYh
=fk4s
-----END PGP SIGNATURE-----
