-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: susv2
Version: 1.1
Binary: susv2
Maintainer: Jeff Bailey <jbailey@raspberryginger.com>
Architecture: all
Standards-Version: 3.6.1
Build-Depends-Indep: debhelper (>> 4.0.0), cdbs
Files: 
 185619e8d4bde70d6ebd2e8ca20d9aca 1552 susv2_1.1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iQCVAwUBQi3DIvtEPvakNq0lAQJ8nQP/TZJq8eYjkaU2KkcD/YfG/FZxljLhZgRR
XxeCdOEBQsdJutG/2//VHUm2G4B7SUfnXvPAltFDGrjIaM9LV3yqPZ0lThP41qUR
R0Hvw//zSZbvE1EQQ1UmXpHRgMVXrl21IRA17VNH6vvXfpA1loPnGyM0COKBZ1/f
QgGHDWB1utA=
=d4xc
-----END PGP SIGNATURE-----
