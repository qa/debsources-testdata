-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: aspell-is
Version: 0.51-0-4
Binary: aspell-is
Maintainer: Brian Nelson <pyro@debian.org>
Architecture: all
Standards-Version: 3.6.2
Build-Depends: debhelper (>> 4.0.0), cdbs (>= 0.4.0), dictionaries-common-dev (>= 0.9.1)
Files: 
 990c08feff2ee1e706b30ed198ae14bf 410578 aspell-is_0.51-0.orig.tar.gz
 56f8d92f45ecadbb21a584239ed90f02 1831 aspell-is_0.51-0-4.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFC4H7h1Ng1YWbyRSERAsEqAJ4wObaWaMwrWHDuW5zs9ka4OeWiHwCfR8Di
h9x1oQ24zntmkjXTPQdEqIk=
=W1qo
-----END PGP SIGNATURE-----
