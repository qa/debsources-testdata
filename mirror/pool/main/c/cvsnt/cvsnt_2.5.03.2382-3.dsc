-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: cvsnt
Version: 2.5.03.2382-3
Binary: cvsnt
Maintainer: Andreas Tscharner <andy@vis.ethz.ch>
Architecture: any
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 4.0.0), autotools-dev, zlib1g-dev, libexpat1-dev, libssl-dev, libkrb5-dev, comerr-dev, libpcre3-dev, unixodbc-dev, libpq-dev, libpam0g-dev, libsqlite3-dev
Uploaders: Christian Bayle <bayle@debian.org>
Files: 
 c50c2d82aeb274a664d8d1cf53ccd0da 6804247 cvsnt_2.5.03.2382.orig.tar.gz
 5d61884ae7a71908f3d69965957c9f02 122556 cvsnt_2.5.03.2382-3.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFFarQ8mdOZoew2oYURAjerAJ9UUP2wbVVaPyfnzEQf2vTfEsuBeACgg7LP
OU2jkBH00jTPV5Si9XXyQ6Y=
=bHWo
-----END PGP SIGNATURE-----
