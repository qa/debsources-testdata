-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: gnubg
Binary: gnubg, gnubg-data
Architecture: any all
Version: 0.90+20120429-1
Maintainer: Russ Allbery <rra@debian.org>
Homepage: http://www.gnubg.org/
Standards-Version: 3.9.3
Vcs-Browser: http://git.eyrie.org/?p=debian/gnubg.git
Vcs-Git: git://git.eyrie.org/debian/gnubg.git
Build-Depends: debhelper (>= 9), bison, dblatex, dh-autoreconf, docbook2x, docbook-xsl, flex, libcairo2-dev, libcanberra-gtk-dev, libfreetype6-dev, libglib2.0-dev, libgmp-dev, libgtk2.0-dev, libgtkglext1-dev, libncurses-dev, libpng-dev, libreadline-dev, libsqlite3-dev, python-dev, texinfo, xsltproc
Package-List: 
 gnubg deb games optional
 gnubg-data deb games optional
Checksums-Sha1: 
 f4c29c93e78148e6b94a6c71a1340aef46463945 14277298 gnubg_0.90+20120429.orig.tar.gz
 2cee932311db4dffa0c86dbb89b1fe09e527f110 27728 gnubg_0.90+20120429-1.debian.tar.gz
Checksums-Sha256: 
 ae6390d02c957a91d65b812de798ccd7918afb1279b2e2bebcec5757d26d98fc 14277298 gnubg_0.90+20120429.orig.tar.gz
 4971d65db8540f6dbac533bdba1144d96dc7961d4d05a3f3538b766ec5585e35 27728 gnubg_0.90+20120429-1.debian.tar.gz
Files: 
 4ed68c66edd18a8ba92d4cd9107860b3 14277298 gnubg_0.90+20120429.orig.tar.gz
 b581dc38eebc30c9f9016338e7d00f30 27728 gnubg_0.90+20120429-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQEcBAEBCAAGBQJPnL3rAAoJEH2AMVxXNt517MIH/3MdFj0CvsUGGJG6CJ8XQeCe
RHAKT+Xqhn+Z2yqClADFHFUBdOvIE6/zeIW47BSrX4U316GvNLSpOcJ6eT2Ispbh
PnbZ2B50uheXTjosMB9K5Y11hWxhLl2ZrzzQEuScP/sphb6NwVIp/b8O68sCOltO
nUDQ5z9SnFutsfA8oXZPmkutNblKGrp7jhY8+dEUf2sbVEnjlXcaE0e09vh46vk+
d7qJKILnI9PRpOgIfAQ1k+TeTbi96BjXqiFMAkWMOJMFqYt730PKdz6U7wVvfJEm
HUAw5ckawE/sL/G3ZhMT1fQqqCAT7Qp5pcwEMEGHzqf1E1KqWIqVfE8I6zJpN/Y=
=f9wa
-----END PGP SIGNATURE-----
