-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: gnubg
Binary: gnubg, gnubg-data
Architecture: any all
Version: 1.02.000-2
Maintainer: Russ Allbery <rra@debian.org>
Homepage: http://www.gnubg.org/
Standards-Version: 3.9.4
Vcs-Browser: http://git.eyrie.org/?p=debian/gnubg.git
Vcs-Git: git://git.eyrie.org/debian/gnubg.git
Build-Depends: debhelper (>= 9), bison, dh-autoreconf, flex, libcairo2-dev, libcanberra-gtk-dev, libfreetype6-dev, libglib2.0-dev, libgmp-dev, libgtk2.0-dev, libgtkglext1-dev, libncurses-dev, libpng-dev, libreadline-dev, libsqlite3-dev, lsb-release, python-dev
Build-Depends-Indep: dblatex, docbook2x, docbook-xsl, texinfo, xsltproc
Package-List: 
 gnubg deb games optional
 gnubg-data deb games optional
Checksums-Sha1: 
 cf725a463967dccf9399493bed3db4fbb7baf88d 14985346 gnubg_1.02.000.orig.tar.gz
 631df079007f50eeab3befd781f3f258e23b02ce 26994 gnubg_1.02.000-2.debian.tar.gz
Checksums-Sha256: 
 7ece7fc02481f8e6c08869306f1cf52cad9ec5ddd675de67782cf028a6dcb504 14985346 gnubg_1.02.000.orig.tar.gz
 b6d5b68c3baf003e1525dfe2429ebae4b5022ca5f24e70d8d867e4cebbb4487d 26994 gnubg_1.02.000-2.debian.tar.gz
Files: 
 eef4d08e20f1de40336de1bb3c9cb9ca 14985346 gnubg_1.02.000.orig.tar.gz
 d807f85fccf28469e787353c59143288 26994 gnubg_1.02.000-2.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.14 (GNU/Linux)

iQEcBAEBCAAGBQJSRO6kAAoJEH2AMVxXNt51NwkIAK6JxvtcWKFapA67dDk4aGOx
VOdt9uV1NCChkFbZFevKUUNLKc3vkdpHwwAuee/adMljoHwLW5ZdXlHO9Enw/6cw
hV62glvMibhCizrH3BQXmG5yznf5CJISzx1ElBTqtvSHJZhYKLXHZUsMGB9kjGtM
H1W3fDEEGel2MoOgdyO/AxPARq5ONERTs9/witG9+t3B9IVJaGSXcSlv2Piwfdsg
a9DZPCcysIY1GxZ/V8upJb2uNz5m+oEFEgP+7q6a+ijgqJFT/4nOg/N986Cn3Ik3
JFzir3PqUJuBtHzjz3GN5ZBNmhqWeUwhktMPGx4mktR/0o0VDwjBZ2xmFfGEZts=
=6FHV
-----END PGP SIGNATURE-----
