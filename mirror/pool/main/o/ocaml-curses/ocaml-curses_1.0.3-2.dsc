-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: ocaml-curses
Binary: libcurses-ocaml, libcurses-ocaml-dev
Architecture: any
Version: 1.0.3-2
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Samuel Mimram <smimram@debian.org>, Sylvain Le Gall <gildor@debian.org>
Homepage: http://www.nongnu.org/ocaml-tmk/
Standards-Version: 3.9.2
Vcs-Browser: http://git.debian.org/?p=pkg-ocaml-maint/packages/ocaml-curses.git
Vcs-Git: git://git.debian.org/git/pkg-ocaml-maint/packages/ocaml-curses.git
Build-Depends: debhelper (>= 7), dpkg-dev (>= 1.13.19), libncursesw5-dev, ocaml-nox (>= 3.11), dh-ocaml (>= 0.9.1), ocaml-findlib (>= 1.2.4)
Package-List: 
 libcurses-ocaml deb ocaml optional
 libcurses-ocaml-dev deb ocaml optional
Checksums-Sha1: 
 6bcb4a6eaf8353aac93069be0084f833a55340c1 54053 ocaml-curses_1.0.3.orig.tar.gz
 8066638e9a52a7b5155389b9aec540220602c364 41139 ocaml-curses_1.0.3-2.debian.tar.gz
Checksums-Sha256: 
 990a55ff99223edaa04387802907b00c475b46dd921dc5f8c5ede15ac673656f 54053 ocaml-curses_1.0.3.orig.tar.gz
 757bf3284fd5542c8ac8a4fab372019e60688acb5506f1c389cbb51e3458e73d 41139 ocaml-curses_1.0.3-2.debian.tar.gz
Files: 
 3c11b46b7c057f8fd110ace319589877 54053 ocaml-curses_1.0.3.orig.tar.gz
 74703e6b29865374bd5dbda067e91828 41139 ocaml-curses_1.0.3-2.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)

iQEcBAEBAgAGBQJPBQDpAAoJEDe1GR0FRlJoKv0IAIKP5mT4ha2fZ4pFvrvF7NOO
ppAtPcLGWCH9dH+imTYaACBhjs8CVhEE0h2x1+Nm3pIhWC5JR2MeF//xUF3UDVym
9GUvHIEpouoTxgG4Z5TQbXlPNg+foIIPWAb1ybSDl5YUSagwHeTG4kdoTUlnnR3y
r/XdiA6Oylbu411c/2Lf8lk5SAQWXOITm1uAWzc04eJZJzXhRF++kB/fe4QMwF+g
CFp/Y3Un9ewpzvOGw+LspKH8Sgio/ihLM6RCl83ClTV/90qdXPfMPZXsLkJ+Urzi
c6tbCoVSSm83JRJtN7et6w7rxubUUwwaue3TuHtACzEG6n6qESAqe8f2oSttJig=
=MvdF
-----END PGP SIGNATURE-----
