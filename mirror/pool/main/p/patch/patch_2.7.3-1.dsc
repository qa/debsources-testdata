-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: patch
Binary: patch
Architecture: any
Version: 2.7.3-1
Maintainer: Laszlo Boszormenyi (GCS) <gcs@debian.org>
Homepage: http://savannah.gnu.org/projects/patch/
Standards-Version: 3.9.5
Vcs-Browser: http://git.debian.org/?p=collab-maint/patch.git
Vcs-Git: git://git.debian.org/collab-maint/patch.git
Build-Depends: debhelper (>= 7), ed
Package-List:
 patch deb vcs standard arch=any
Checksums-Sha1:
 4191a36e4733935912280650b32644d9c786dfa1 684764 patch_2.7.3.orig.tar.xz
 f55e05a44ce413bad4ec4024b1535642a32bb49e 8008 patch_2.7.3-1.debian.tar.xz
Checksums-Sha256:
 d09022de9d629561bf4dad44625ef4b1ead15178b210412113531730cdb6f19d 684764 patch_2.7.3.orig.tar.xz
 ec7b8b549a0ae8a00edd4655715100e22d85c3f3babc7c83ee0008cc23093632 8008 patch_2.7.3-1.debian.tar.xz
Files:
 29b87be845e4662ab0ca0d48a805ecc6 684764 patch_2.7.3.orig.tar.xz
 ce27aa99309c2c801fd6f9bcc951aa2c 8008 patch_2.7.3-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBCAAGBQJUw8gIAAoJENzjEOeGTMi/G0sP/RCLohULZ10SFevIFAfnBkLr
nte+7Y1BMLpprR/6b9dZaYCNCRtGMYbnulhn3XTJzaeTHRka7D429IroZ1r0hp8P
Xaw0klXbVkMWnllYRMWVvC7klyzfu63cKApm7L0uROkx+2lW5U5GI78kBH9mqhIt
yt5MkbUQlSs7kRrrFLz3euN6iyJQj8cTO5a4ERqIvFzEORbSU9Xo4QqlY1OwxTfV
Sh1DncmY9GIeTF5F+9L1Iq5QXVqRivv0Eh5iRda8WB39OPzo1pCrhgzRvjOGPfhm
YwuWGHF2GEjqz880avFvQd6/7Hho/tBmbpt9vlxlpmhF/KvLf9N/ey0Bkm5DG7yP
lFFIjaXq0Mf+zncRYq2A6CHubFOk8qnTD4/aL6vIDzIiaRSzMxzFtgwWTeNCHe+n
a2dVpMljAAomt28jNCXOKeol1+ZYX3pahiUAEb1qiZgrt0TJhlCMMX/w+tKiFi3g
xB0ZOBFG6xYBC3LzIlWnrvzU6E1szXPrkZTsVgWpBLgRcwYK1v6lgEaWLSAJvKWP
r889NOp8XRoQo2YH7ExYPeEHUkXm01l8+a0QS0DXColqG292C+6rzuQU51CI81NG
BJ4rQ+yc9Y/4kTp202xvOy3+GuQjl96q+TOF4KZV6IYJtshHPO00MGmnlF6WeUVS
Z99UH90uQswW9edj1b9F
=Be6P
-----END PGP SIGNATURE-----
