-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: bsdgames-nonfree
Binary: bsdgames-nonfree
Architecture: any
Version: 2.17-3
Maintainer: Aaron M. Ucko <ucko@debian.org>
Standards-Version: 3.8.0
Build-Depends: debhelper (>> 7), dpkg-dev (>= 1.14.17), libncurses5-dev
Checksums-Sha1: 
 001d8d299643bd7654f277dc0f1437831690dd8b 181134 bsdgames-nonfree_2.17.orig.tar.gz
 55688cc8393ca3bb981fba5c45e9415743b286dc 6432 bsdgames-nonfree_2.17-3.diff.gz
Checksums-Sha256: 
 912a9253791ec9748dc49ab6cf7aacf417483c50ee04db57ed6d5dca110cb563 181134 bsdgames-nonfree_2.17.orig.tar.gz
 6721a1b65ab7dfeb566e834909c12292815d0b35c729be12b6a2d424d4af796a 6432 bsdgames-nonfree_2.17-3.diff.gz
Files: 
 f70b5fe38c3dd82b44024263819621f9 181134 bsdgames-nonfree_2.17.orig.tar.gz
 dd4ba1c27b3ac68c9aaef4e5c2e3bd22 6432 bsdgames-nonfree_2.17-3.diff.gz
Autobuild: yes

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)

iEYEARECAAYFAkhUQiIACgkQWNCxsidXLEe+CACfQKRLl6eV+tIHlz3QtEdUioDD
cr8AoLsGcGaFa0P4DJk4RUAB/8EmBtb4
=DDR3
-----END PGP SIGNATURE-----
