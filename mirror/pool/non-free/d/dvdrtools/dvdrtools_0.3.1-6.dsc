-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: dvdrtools
Binary: dvdrtools
Architecture: any
Version: 0.3.1-6
Maintainer: Kartik Mistry <kartik.mistry@gmail.com>
Homepage: http://savannah.nongnu.org/projects/dvdrtools/
Standards-Version: 3.7.3
Build-Depends: debhelper (>= 5), dpatch
Files: 
 5707b7e877b853e258cd738938833006 1068060 dvdrtools_0.3.1.orig.tar.gz
 992cb839bbe7afbbf08e8d9fe21c7a55 15557 dvdrtools_0.3.1-6.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.6 (GNU/Linux)

iD8DBQFHdiHepGK1HsL+5c0RAlJdAKDXbuK76ur+rJKvwrvn5LyH5kxnpwCdH8/T
gMrz/UjclsaaWTmMme9Knn4=
=kqa9
-----END PGP SIGNATURE-----
