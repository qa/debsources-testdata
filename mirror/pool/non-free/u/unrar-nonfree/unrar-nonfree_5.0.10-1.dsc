-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: unrar-nonfree
Binary: unrar
Architecture: any
Version: 1:5.0.10-1
Maintainer: Martin Meredith <mez@debian.org>
Homepage: http://www.rarlabs.com/
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 9), dpkg-dev (>= 1.16.1~)
Package-List: 
 unrar deb non-free/utils optional
Checksums-Sha1: 
 d48c245a58193c373fd2633f40829dcdda33b387 208341 unrar-nonfree_5.0.10.orig.tar.gz
 205bd16a6bcd36098f3634e3735df5c28aa4e78f 5901 unrar-nonfree_5.0.10-1.debian.tar.gz
Checksums-Sha256: 
 05e4451d49eee36f141f24f7eb48c4886e84d54e98db9e64f74e8fe50ab80dc1 208341 unrar-nonfree_5.0.10.orig.tar.gz
 624da3d01120a9e82db8ce17039c341af2fbc1d2436174c7d25c6341354f1a9b 5901 unrar-nonfree_5.0.10-1.debian.tar.gz
Files: 
 9bec605946c0d7e8b0534a77577fa2fb 208341 unrar-nonfree_5.0.10.orig.tar.gz
 7e98475a3f58a055ad86abe47e9cc45a 5901 unrar-nonfree_5.0.10-1.debian.tar.gz
Autobuild: yes

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBAgAGBQJSDQrrAAoJECr71n/RM6xuA7AP/ipVzBE4vvBK91bLZ10Owg5l
1ioUmIChFdsnXIdoFLZW4lZuUBsvGhiwXFbiD5Y6HeyazD1l4wuGo0MEUIGgtRsv
M3ZwM1XYdwU1DRrDyCITEaxNrXWm5ftLcXWhXqGrpVSWZsyBlcn9PwQ0mkV7erwk
6TqILJgIC+TYVE1486mqdihEl7VilS8KmN7fSaVGG74wX42MTps1CXg1BjhNteWb
I+5WusZdnTDys2cNJDXukoVnNzlI+BymSMJxAvZDQYdjSPiW1db6bcVGXxrsqdeL
6UjTCH3T2SCqHBdeSxgvI2qikXMDGdIWwQm8rGpv83qCWK76Qpxy4UeikOm9xJMc
3LWYjZkiVGOhkZm7U4uBzuEXFrcZCov2BsDJZvfV4HW9etf99KCwAEvkr63CTebc
lqihKjeeBD9Y+xsVTscUK02vgOw5xR5NQtDLlS9jW5NS/6F7mEaUkv5t+Wtu5hWR
FGTJfIIXGTf4gSyUVp5EICpQ5HIyUVMZQi2pjq0tpTRisuWW6oFuN0AFQwts41QP
IFuZHo21kH36LegwzP/g/kkKFuD0YlboyuWQ9cOFgLC+qPsA0u4bT+c/3Ei+i/XN
tGnrmkYBaeMPi34kJdDp6sWkM2h/drgKCsVCxr/hToe83/A25lzu9oe7fzS4249a
FHkNlJRZc+X+a/xfEvX1
=PqhL
-----END PGP SIGNATURE-----
