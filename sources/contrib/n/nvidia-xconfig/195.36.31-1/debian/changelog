nvidia-xconfig (195.36.31-1) unstable; urgency=low

  [ Andreas Beckmann ]
  * Team change: switch Uploaders from Randall to Russ.
  * New upstream release.  (Closes: #479741)
  * Switch to source format 3.0 (quilt).
  * Update to debhelper compat level 7 and Standards-Version 3.8.4.
  * debian/rules: switch to dh and minimize.
  * Adjust Suggests and add nvidia-glx-legacy-* as alternatives.
  * Add watch file.
  * Rephrase long description.  (Closes: #494917)
  * Switch debian/copyright to DEP-5 format and add more license statements
    found in the source.
  * Add Vcs-* URLs.

  [ Russ Allbery ]
  * Move this package to contrib/x11 and priority extra.  It is only
    useful with the non-free NVIDIA drivers since it forces the module to
    the non-free version, and it is now rarely necessary due to the
    improved X.org auto-configuration support.
  * Increase Suggests on the nvidia-glx packages to Recommends, now
    possible with the package moved to contrib, since the library provided
    there is required for some functionality.  (Closes: #356800)
  * Add some additional information to debian/copyright about NVIDIA's
    interpretation of their licensing terms and the use of the GPL for
    code derived from LGPL code.
  * Rewrap and expand the package long description, and make it clearer in
    the short description it's only used with the non-free drivers.
  * Only build on i386 and amd64, since this package is useless without
    NVIDIA hardware.
  * Pass the correct options to the upstream Makefile to disable stripping
    if nostrip is set in DEB_BUILD_OPTIONS.  (Closes: #437655)

  [ Daniel Baumann ]
  * Add a Homepage control field pointing to the download directory.

 -- Russ Allbery <rra@debian.org>  Sat, 12 Jun 2010 23:01:11 -0700

nvidia-xconfig (1.0+20080522-2) unstable; urgency=high

  * add missing pkg-config dependancy. (closes: #497564)
  * change maintainer and uploaders.

 -- Randall Donald <rdonald@debian.org>  Fri, 05 Sep 2008 14:52:57 -0700

nvidia-xconfig (1.0+20080522-1) unstable; urgency=low

  * new upstream

 -- Randall Donald <rdonald@debian.org>  Tue, 24 Jun 2008 10:53:38 -0700

nvidia-xconfig (1.0+20070502-1) unstable; urgency=low

  * New upstream.

 -- Randall Donald <rdonald@debian.org>  Sat, 12 May 2007 15:01:23 -0700

nvidia-xconfig (1.0+20051122-2) unstable; urgency=low

  * Add license and copyright info for XF86Config-parser 

 -- Randall Donald <rdonald@debian.org>  Fri, 30 Dec 2005 10:44:29 -0800

nvidia-xconfig (1.0+20051122-1) unstable; urgency=low

  * Initial Upload.

 -- Randall Donald <rdonald@debian.org>  Mon,  7 Nov 2005 10:54:24 -0800

