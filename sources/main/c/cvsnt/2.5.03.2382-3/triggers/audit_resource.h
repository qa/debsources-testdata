//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by audit_trigger.rc
//
#define IDC_BUTTON2                     1002
#define IDC_COMBO1                      1003
#define IDC_EDIT1                       1004
#define IDC_EDIT2                       1005
#define IDC_EDIT3                       1006
#define IDC_EDIT4                       1007
#define IDC_CHECK1                      1008
#define IDC_CHECK2                      1009
#define IDC_CHECK3                      1010
#define IDC_CHECK4                      1011
#define IDC_CHECK5                      1012
#define IDC_CHECK6                      1013
#define IDD_DIALOG1                     1014
#define IDC_BUTTON1                     1014
#define IDC_EDIT5                       1015
#define IDC_SMTPINTERNAL                2036
#define IDC_SMTPEXTERNAL                2037
#define IDC_DOMAINNAME                  2038
#define IDC_COMMANDNAME                 2040
#define IDC_SMTPSERVERNAME              2053

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
