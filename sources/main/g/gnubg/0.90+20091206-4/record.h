/*
 * record.h
 *
 * by Gary Wong <gtw@gnu.org>, 2002.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 3 or later of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: record.h,v 1.9 2009/03/24 23:48:46 c_anthon Exp $
 */

#ifndef RECORD_H
#define RECORD_H

typedef enum _expaverage {
    EXPAVG_TOTAL, EXPAVG_20, EXPAVG_100, EXPAVG_500
} expaverage;
#define NUM_AVG ((int)EXPAVG_500 + 1)

typedef struct _playerrecord {
    char szName[ MAX_NAME_LEN ];
    int cGames;
    double arErrorChequerplay[ NUM_AVG ];
    double arErrorCube[ NUM_AVG ];
    double arErrorCombined[ NUM_AVG ];
    double arLuck[ NUM_AVG ];
} playerrecord;

extern int RecordReadItem( FILE *pf, char *pch, playerrecord *ppr );

extern gboolean records_exist(void);

#define GNUBGPR "gnubgpr"

#endif
