#!/usr/bin/make -f

# Enable compiler hardening flags.
export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow

# Add --as-needed to drop a few unnecessary dependencies.
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# These variable is used only by get-orig-source, which will normally only be
# run by maintainers.  Update the version in debian/changelog before running
# this target.  The snapshot date will be extracted from the version, which
# should be in the form <version>+<date>.
VERSION = $(shell dpkg-parsechangelog | grep ^Version: | cut -d' ' -f2 \
		| cut -d- -f1)
DATE    = $(shell echo "$(VERSION)" | cut -d+ -f2)
URL     = http://www.gnubg.org/media/sources/
FILE    = gnubg-source-SNAPSHOT-$(DATE).tar.gz

# Only build with SSE on amd64.  We can't assume that processor for i386,
# and it fails to build on ia64 since GCC there doesn't support -msse or
# -msse2 (as of 2010-01-17).
DEB_HOST_ARCH_CPU ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_CPU)
ifeq ($(DEB_HOST_ARCH_CPU),amd64)
    SSE = --enable-sse
else
    SSE = --disable-sse
endif

# Download the upstream source and clean out the CVS subdirectories and
# corecount.c, which is not under a free license.
get-orig-source:
	wget $(URL)$(FILE)
	tar xfz $(FILE)
	rm $(FILE)
	mv gnubg gnubg_$(VERSION).orig
	rm gnubg_$(VERSION).orig/corecount.c
	find gnubg_$(VERSION).orig -name CVS -print0 | xargs -r -0 rm -r
	tar cf gnubg_$(VERSION).orig.tar gnubg_$(VERSION).orig
	rm -r gnubg_$(VERSION).orig
	gzip -9 gnubg_$(VERSION).orig.tar

%:
	dh $@ --with autoreconf

override_dh_autoreconf:
	dh_autoreconf --as-needed

override_dh_auto_configure:
	env dh_auto_configure -- --with-gtk --with-board3d --with-sqlite \
	    --with-python --enable-threads $(SSE)

# Disable building the rollout databases during an architecture-specific
# build.  It takes too long and is too large.  We'll instead build the
# one-sided database only during an arch-independent build and buid the
# two-sided database during installation.
#
# It would be nice to be able to re-enable this build with an -indep
# override of the same target, but currently the buildds call both, so we
# have to handle that by overriding the binary target.
override_dh_auto_build-arch:
	echo 'stop' > gnubg_ts0.bd
	echo 'stop' > gnubg_os0.bd
	dh_auto_build

override_dh_auto_build-indep:
	echo 'stop' > gnubg_ts0.bd
	dh_auto_build

# binary calls dh_prep first thing, so here's where we can remove our fake
# file.  dh_auto_install will then call make, which will build the file.
override_dh_prep-indep:
	rm -f gnubg_os0.bd
	dh_prep

override_dh_auto_install:
	$(MAKE) install DESTDIR=$(CURDIR)/debian/gnubg
	rm debian/gnubg/usr/share/gnubg/gnubg_ts0.bd
	rm -r debian/gnubg/usr/share/gnubg/fonts
	chmod 755 debian/gnubg/usr/share/gnubg/scripts/query_player.sh
	perl -i -pe 's/^(gnubg\s+)-(bcd\S+)/$$1\\fB\\-$$2\\fR/' \
	    debian/gnubg/usr/share/man/man6/gnubg.6
	mv debian/gnubg/usr/bin debian/gnubg/usr/games
	install -d debian/gnubg/usr/share/applications
	install -m 644 debian/gnubg.desktop \
	    debian/gnubg/usr/share/applications/gnubg.desktop
	install -d debian/gnubg/var/lib/gnubg
	install -d debian/gnubg/usr/lib/gnubg
	install -d debian/gnubg-data/usr/share
	mv debian/gnubg/usr/share/gnubg debian/gnubg-data/usr/share/
	mv debian/gnubg-data/usr/share/gnubg/gnubg.wd \
	    debian/gnubg/usr/lib/gnubg/gnubg.wd
	install -d debian/gnubg/usr/share/gnubg
	ln -s /usr/lib/gnubg/gnubg.wd \
	    debian/gnubg/usr/share/gnubg/gnubg.wd
	mv debian/gnubg/usr/share/locale debian/gnubg-data/usr/share/
	install -d debian/gnubg-data/usr/share/gnubg/fonts
	ln -s ../../fonts/truetype/ttf-dejavu/DejaVuSans.ttf \
	    debian/gnubg-data/usr/share/gnubg/fonts/Vera.ttf
	ln -s ../../fonts/truetype/ttf-dejavu/DejaVuSans-Bold.ttf \
	    debian/gnubg-data/usr/share/gnubg/fonts/VeraBd.ttf
	ln -s ../../fonts/truetype/ttf-dejavu/DejaVuSerif-Bold.ttf \
	    debian/gnubg-data/usr/share/gnubg/fonts/VeraSeBd.ttf
	install -d debian/gnubg-data/usr/share/doc/gnubg
	mv debian/gnubg/usr/share/doc/gnubg/images \
	    debian/gnubg-data/usr/share/doc/gnubg/
	mv debian/gnubg/usr/share/doc/gnubg/*.html \
	    debian/gnubg-data/usr/share/doc/gnubg/
	install -m 644 doc/*.pdf debian/gnubg-data/usr/share/doc/gnubg/

override_dh_installchangelogs:
	dh_installchangelogs -i ChangeLog
	dh_installchangelogs -a -XChangeLog
	[ ! -f debian/gnubg-data/usr/share/doc/gnubg-data/changelog ] \
	    || mv debian/gnubg-data/usr/share/doc/gnubg-data/changelog \
		debian/gnubg-data/usr/share/doc/gnubg/changelog

# Do not compress the PDF documentation.
override_dh_compress:
	dh_compress -X.pdf
