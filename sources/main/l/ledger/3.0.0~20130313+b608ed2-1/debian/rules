#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# hardening flags (CPPFLAGS kludge is for CMake)
export CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
export CFLAGS:=$(shell dpkg-buildflags --get CFLAGS) $(CPPFLAGS)
export CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS) $(CPPFLAGS)
export LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS)

# avoid linking ledger binary to extra libs.
LDFLAGS += -Wl,--as-needed

# Variables for generating snapshots. These will be empty or wrong if
# git is not present. Do not rely on them in required targets.

UPSTREAM_DATE=$(shell git log -1 --date=short --pretty="format:%cd" upstream | tr -d -)
UPSTREAM_HASH=$(shell git rev-parse --short upstream)
UPSTREAM_VERSION=3.0.0~${UPSTREAM_DATE}+${UPSTREAM_HASH}

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
ifneq ($(DEB_HOST_GNU_TYPE),$(DEB_BUILD_GNU_TYPE))
CROSS= --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
else
CROSS= --build $(DEB_BUILD_GNU_TYPE)
endif

BUILDDIR=obj-$(DEB_BUILD_GNU_TYPE)

config-stamp:
	mkdir -p $(BUILDDIR)
	cd $(BUILDDIR) && \
	    cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_VERBOSE_MAKEFILE=ON \
		-DBUILD_WEB_DOCS=1
	touch $@

build-indep build-arch: build

build: build-stamp

build-stamp:  config-stamp
	dh_testdir

	# Add here commands to compile the package.
	$(MAKE) -C $(BUILDDIR)
	$(MAKE) -C $(BUILDDIR)/doc doc
	cd $(BUILDDIR)/doc && makeinfo ../../doc/ledger.texi

	touch $@

clean: 
	dh_testdir
	dh_testroot

	# Add here commands to clean up after the build process.
	dh_auto_clean

	dh_clean 

install: build
	dh_testdir
	dh_testroot
	dh_prep 
	dh_installdirs

	# Add here commands to install the package into debian/ledger.
	$(MAKE) -C$(BUILDDIR) DESTDIR=$(CURDIR)/debian/ledger install
	cp $(BUILDDIR)/doc/ledger.pdf $(CURDIR)/debian/ledger/usr/share/doc/ledger
	cp -a $(BUILDDIR)/doc/ledger.html $(CURDIR)/debian/ledger/usr/share/doc/ledger

	# Clean out some stuff that make install puts in that we don't want
	rm -rf $(CURDIR)/debian/ledger/usr/include
	rm -rf $(CURDIR)/debian/ledger/usr/lib
	rm -rf $(CURDIR)/debian/ledger/usr/share/info/dir

	# Install emacs mode for ledger.  Not byte compiled.  
	cp -a lisp/*.el $(CURDIR)/debian/ledger/usr/share/emacs/site-lisp

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs
	dh_installdocs
	dh_installemacsen
	dh_installexamples
	dh_installinfo $(BUILDDIR)/doc/ledger.info
	dh_installman
	dh_strip
	dh_compress -X.pdf
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch

get-orig-source: 
	bash debian/get-orig-source.sh ${UPSTREAM_VERSION} ${UPSTREAM_HASH}

dch-snapshot:
	dch -v "${UPSTREAM_VERSION}-1" "Updated git snapshot for experimental."

.PHONY: build clean binary-indep binary-arch binary install get-orig-source
