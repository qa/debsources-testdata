Source: ledit
Section: editors
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Samuel Mimram <smimram@debian.org>,
 Stefano Zacchiroli <zack@debian.org>,
 Stéphane Glondu <glondu@debian.org>,
 Mehdi Dogguy <mehdi@debian.org>,
 Sylvain Le Gall <gildor@debian.org>
Build-Depends:
 debhelper (>> 7.0.0),
 dh-ocaml (>= 0.9~),
 dpatch,
 ocaml-nox,
 camlp5 (>= 5.12)
Standards-Version: 3.8.4
Homepage: http://cristal.inria.fr/~ddr/ledit/
Vcs-Git: git://git.debian.org/git/pkg-ocaml-maint/packages/ledit.git
Vcs-Browser: http://git.debian.org/?p=pkg-ocaml-maint/packages/ledit.git

Package: ledit
Architecture: all
Depends:
 ${ocaml:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Provides: readline-editor
Description: line editor for interactive programs
 Ledit is a line editor, allowing to use control commands like in emacs
 or in shells (bash, tcsh). To be used with interactive commands. It is
 written in OCaml and Camlp4 and uses the library unix.cma.

Package: libledit-ocaml-dev
Architecture: any
Section: ocaml
Depends:
 ${ocaml:Depends},
 ocaml-findlib,
 camlp5,
 ${shlib:Depends},
 ${misc:Depends}
Provides:
 ${ocaml:Provides}
Description: OCaml line editor library
 Ledit is a line editor, allowing to use control commands like in emacs
 or in shells (bash, tcsh). To be used with interactive commands. It is
 written in OCaml and Camlp4 and uses the library unix.cma.
 .
 This package ships Ledit as a development library, so that you can use
 it to build interactive programs with line editing capabilities.
