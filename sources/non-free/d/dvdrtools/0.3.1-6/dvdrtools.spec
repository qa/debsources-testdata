# If you're building a subversion snapshot, define SVN
# to the SVN revision you're building.
# Use %nil if you are building a released version.
%define SVN %nil

Summary: A collection of CD/DVD utilities.
Name: dvdrtools
Version: 0.3.1
%if "%SVN" == ""
Release: 1ark
Source: %{name}-%{version}.tar.bz2
%else
Release: 0.%SVN.1ark
Source: %{name}-%SVN.tar.bz2
%endif
License: GPL
Group: Applications/System
URL: http://www.arklinux.org/projects/dvdrtools/
BuildRoot: %{_tmppath}/%{name}-%{version}-root

%description
dvdrtools is a collection of CD/DVD utilities.

%package -n dvdrecord
Summary: A command line CD/DVD recording program.
Group: Applications/Archiving
Provides: cdrecord
Obsoletes: cdrecord

%description -n dvdrecord
Dvdrecord is an application for creating audio and data CDs and DVDs.
Dvdrecord works with many different brands of CD/DVD recorders, fully supports
multi-sessions and provides human-readable error messages.

%package -n mkisofs
Summary: Creates an image of an ISO9660 filesystem.
Group: Applications/System
Obsoletes: cdrecord-mkisofs

%description -n mkisofs
The mkisofs program is used as a pre-mastering program; i.e., it
generates the ISO9660 filesystem.  Mkisofs takes a snapshot of
a given directory tree and generates a binary image of the tree
which will correspond to an ISO9660 filesystem when written to
a block device.  Mkisofs is used for writing CD-ROMs, and includes
support for creating bootable El Torito CD-ROMs.

Install the mkisofs package if you need a program for writing
CD-ROMs.

%package -n cdda2wav
Group: Applications/Multimedia
Summary: A utility for sampling/copying .wav files from digital audio CDs.
Obsoletes: cdrecord-cdda2wav

%description -n cdda2wav
Cdda2wav is a sampling utility for CD-ROM drives that are capable of
providing a CD's audio data in digital form to your host. Audio data
read from the CD can be saved as .wav or .sun format sound files.
Recording formats include stereo/mono, 8/12/16 bits and different
rates.  Cdda2wav can also be used as a CD player.

%package video
Group: Applications/Multimedia
Summary: Tools for mastering video DVDs
Requires: transcode

%description video
Tools for mastering video DVDs. At the moment, you can write MPEG and AVI
videos to DVD, but you can not (yet) create DVD menus.

%prep
%if "%SVN" != ""
%setup -q -n trunk
%else
%setup -q
%endif
aclocal
autoheader
automake -a
autoconf
%configure

%build
make %?_smp_mflags

%install
rm -rf "$RPM_BUILD_ROOT"
make install DESTDIR="$RPM_BUILD_ROOT"
ln -s dvdrecord $RPM_BUILD_ROOT%{_bindir}/cdrecord

%clean
rm -rf $RPM_BUILD_ROOT

%files -n dvdrecord
%defattr(-,root,root)
%doc ChangeLog NEWS AUTHORS README*
%{_bindir}/dvdrecord
%{_bindir}/cdrecord
%{_bindir}/readcd
%{_bindir}/devdump
%{_mandir}/man1/readcd.1*
%{_mandir}/man1/dvdrecord.1*

%files -n mkisofs
%defattr(-,root,root)
%doc mkisofs/ChangeLog
%doc mkisofs/README*
%{_bindir}/*iso*
%{_mandir}/man8/*iso*.8*

%files -n cdda2wav
%defattr(-,root,root)
%doc cdda2wav/FAQ cdda2wav/OtherProgs
%doc cdda2wav/README* cdda2wav/Frontends cdda2wav/HOWTOUSE
%{_bindir}/cdda2*
%{_mandir}/man1/cdda2wav.1*

%files video
%defattr(-,root,root)
%{_bindir}/ifogen
%{_bindir}/makecd

%changelog
* Thu Feb 16 2006 Bernhard Rosenkraenzer <bero@arklinux.org> 0.3.0-1ark
- 0.3.0

* Sat Feb  5 2005 Bernhard Rosenkraenzer <bero@arklinux.org> 0.2.1-1ark
- 0.2.1 -- support k3b's dev=ATAPI:/dev/hdd stuff

* Sat Feb  5 2005 Bernhard Rosenkraenzer <bero@arklinux.org> 0.2.0-1ark
- 0.2.0 with much better ATAPI support

* Thu Oct 28 2004 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.7-0.cvs20041028.1ark
- Add preliminary ATAPI support; this is probably the last version before the rewrite

* Sun May 16 2004 Zackary Deems <zdeems@arklinux.org> 0.1.7-0.cvs20040516.1ark
- Modified cdrecord.c and cdrecord.h to make the output compatible with modern versions
- of cdrecord. CD burning frontends (like k3b) expect cdrecord to use 2.x version output
- and our version was not in compliance.  Rather than expecting the frontends to backport
- to an old version of cdrecord, it makes more sense to update our output to comply.

* Thu Nov 27 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.6-0.cvs20031128.1ark
- Allow writing 4 GB files to UDF images

* Sun Nov  9 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.6-0.cvs20031109.1ark
- makecd improvements

* Sat Nov  8 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.6-0.cvs20031108.1ark
- Add speed= DVD patch
- makecd: Accept vob files as input

* Sun Nov  2 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.6-0.cvs20031102.1ark
- Add -T option to prefer transcode over replex

* Sat Nov  1 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.6-0.cvs20031101.1ark
- Use replex in makecd when possible

* Thu Oct 23 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.6-0.cvs20031023.1ark
- Update, fix makecd overwriting CDs/DVDs without confirmation

* Mon Oct 20 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.6-0.cvs20031020.1ark
- Update to get replex stuff

* Wed Oct 08 2003 Sergio Visinoni <piffio@arklinux.org> 0.1.6-0.cvs20031008.1ark
- Update to CVS to get the new makecd

* Wed Jul 16 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.6-0.cvs20030716.1ark
- Update to CVS to get the new ifogen

* Thu Jun 12 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.5-1ark
- 0.1.5 final

* Sat Mar 29 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.5-0.cvs20030329.1ark
- Update

* Fri Mar 28 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.5-0.cvs20030326.2ark
- Add missing makecd script to -video

* Wed Mar 26 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.5-0.cvs20030326.1ark
- Update

* Wed Jan  1 2003 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.3-1ark
- 0.1.3 final
- new -video subpackage

* Sun Dec 29 2002 Bernhard Rosenkraenzer <bero@arklinux.org> 0.1.3-0.cvs20021229.1ark
- Update to CVS for mkisofs -dvd-video option

* Mon Aug 26 2002 Ark Linux Team <arklinux@arklinux.org> 0.1.2-2ark
- automated rebuild

* Fri Jul  5 2002 David Sainty <saint@redhat.com> 0.1.2-2ark
- Extend workaround for automake to man8 for mkisofs man pages

* Thu Jun 13 2002 Bernhard Rosenkraenzer <bero@linux-easy.com> 0.1.2-1ark
- Build everything, obsolete cdrecord

* Thu Mar 14 2002 Bernhard Rosenkraenzer <bero@redhat.com> 0.1.2-1
- initial build for Red Hat Linux; disable cdda2wav and mkisofs
  and readcd because we're getting them from cdrtools for now.
